# k8s sameNode Python

Python script to find pods with same owner if all of them run on same node.
It uses the current kubeconfig/-context to execute on.

## Installation

- check out this git repo and `cd` into it
- run the following to setup python3 venv and actiate it (python3 needs to be installed)

```bash
python3 -m venv .venv
source .venv/bin/activate
pip install kubernetes
```

## Run command

- activate venv or install dependencies globally `source .venv/bin/activate`
- run command `python ./sameNode.py`
