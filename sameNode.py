#!/usr/bin/env python
from kubernetes import client, config
import collections

# load active kubeconfig
config.load_kube_config()

# connect to API and get list of pods
v1 = client.CoreV1Api()
pod_list = v1.list_pod_for_all_namespaces(watch=False)

nodes_per_owner = collections.defaultdict(set)

pod_lookups = {}
pod_ownerless = []
for pod in pod_list.items:
    if (pod.metadata.owner_references):
        # if pods have an owner store information about them
        owner = str(pod.metadata.owner_references[-1].kind) + "/" + str(pod.metadata.owner_references[-1].name)
        nodes_per_owner[owner.lower()].update([pod.spec.node_name])
        pod_lookups[owner.lower()] = pod
    else:
        # if pods don't have an owner store them separately
        pod_ownerless.append([pod.metadata.namespace,pod.metadata.name])

owner_with_one_node = list(map(lambda x: x[0], filter(lambda x: len(x[1]) == 1, nodes_per_owner.items())))


# do some output formatting for pods on same nodes
if (len(owner_with_one_node) > 0):
    # get fields necessary for output formatting
    output_info = []
    for owner_lookup in owner_with_one_node:
        owner_ref = str(pod_lookups[owner_lookup].metadata.owner_references[-1].kind) + "/" + str(pod_lookups[owner_lookup].metadata.owner_references[-1].name)
        output_info.append({"namespace": pod_lookups[owner_lookup].metadata.namespace, "name": pod_lookups[owner_lookup].metadata.name, "node": pod_lookups[owner_lookup].spec.node_name, "owner": owner_ref.lower()})

    
    columns = ["NAMESPACE", "NAME", "NODE", "OWNER"]
    col_width_data = max(len(value) for row in output_info for key, value in row.items()) + 2
    col_width_columns = max(len(word) for word in columns) + 2
    col_width = col_width_columns if (col_width_columns > col_width_data) else col_width_data
    print("There are", len(output_info), "pods from same owner that are all running on the same node")
    print ("".join(word.ljust(col_width) for word in columns))
    for pod in output_info:
        print ("".join(value.ljust(col_width) for key, value in pod.items()))
else:
    print("No pods from same owner running exclusively on same node")

# do some output formatting for pods without owner
print("\n")
if (len(pod_ownerless) > 0):
    print("The following pods are ownerless")
    columns = ["NAMESPACE", "NAME"]
    col_width_data = max(len(word) for row in pod_ownerless for word in row) + 2
    col_width_columns = max(len(word) for word in columns) + 2
    col_width = col_width_columns if (col_width_columns > col_width_data) else col_width_data
    print ("".join(word.ljust(col_width) for word in columns))
    for pod in pod_ownerless:
        print ("".join(word.ljust(col_width) for word in pod))
else:
    print("No pods are ownerless")
